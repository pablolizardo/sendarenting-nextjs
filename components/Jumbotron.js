import React from "react";

function Jumbotron({ title, bajada }) {
  return (
    <div>
      <h2>{title}</h2>
      <p>{bajada}</p>
    </div>
  );
}

export default Jumbotron;
