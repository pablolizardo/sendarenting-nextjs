import Link from 'next/link'

function Header() {
    const links = [
        { to : '/', title : 'Inicio', icon:'', color: 'yellow'},
        { to : 'nosotros', title : 'Nosotros', icon:'', color: 'blue'},
        // { to : 'tarifas', title : 'Tarifas', icon:'', color: 'blue'},
        { to : 'servicios', title : 'Servicios', icon:'', color: 'yellow'},
        { to : 'clientes', title : 'Clientes', icon:'', color: 'yellow'},
        { to : 'contacto', title : 'Contacto', icon:'', color: 'yellow'},
    ]
    return (
        <header>
        <div className='container'>
            <img src='logo.png' alt='logo Senda Renting'/>
            <nav>
                <ul>
                    {links.map(link => <li><Link href={link.to}><a>{link.title}</a></Link></li> )}
                </ul>
            </nav>
            </div>
        </header>
    )
}

export default Header
