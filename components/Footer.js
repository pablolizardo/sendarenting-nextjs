import React from "react";

function Footer() {
  const data = {
    telefonos: [
      {
        link:
          "https://wa.me/5492964465099?text=Me%20gustaría%20saber%20el%20precio%20del%20coche",
        title: " +54 2964 15465099",
        icon: "whatsapp",
      },
      {
        link:
          "https://wa.me/5492964497861?text=Me%20gustaría%20saber%20el%20precio%20del%20coche",
        title: " +54 2964 15497861",
        icon: "whatsapp",
      },
      {
        link:
          "https://wa.me/5492964619695?text=Me%20gustaría%20saber%20el%20precio%20del%20coche",
        title: " +54 2964 15619695",
        icon: "whatsapp",
      },
      {
        link: "tel:+54 2964 431958",
        title: " +54 2964 431958",
        icon: "phone fa",
      },
    ],
    direcciones: [
      {
        link: "mailto:sendarenting@gmail.com",
        title: "sendarenting@gmail.com",
        icon: "envelope fa",
        label: "Email",
      },
      {
        link: "https://goo.gl/maps/W7Qbtqk2vr1NF8Ne6",
        title: "9 de Julio 679. Local 20",
        icon: "map-pin fa",
      },
      {
        link: "https://g.page/aa2000-rga?share",
        title: "Aeropuerto Argentina 2000",
        icon: "map-pin fa",
      },
    ],
    socials: [
      {
        link: "https://www.facebook.com/SendaRenting/",
        title: "Facebook",
        icon: "facebook",
      },
      {
        link: "https://www.instagram.com/sendasrentingrg/",
        title: "Instagram",
        icon: "instagram",
      },
      // {link: 'https://g.page/aa2000-rga?share', title : 'Google', icon:'google'},
    ],
  };
  return (
    <footer className='container'>

      <ul>
        {data.telefonos.map((telefono) => <li><a href={`tel:${telefono.title}`}>{telefono.title}</a></li> )}
      </ul>
      <ul>
        {data.socials.map((social) => <li><a href={`tel:${social.title}`}>{social.title}</a></li> )}
      </ul>
      <ul>
        {data.direcciones.map((direccion) => <li><a href={`map:${direccion.title}`}>{direccion.title}</a></li> )}
      </ul>
      <p> &copy; 2013-2019 Senda Renting. Rio Grande Tierra del Fuego - Patagonia - Argentina </p>
      <p> Diseño y desarrollo por <a href="http://www.soki.com.ar"> SOKI </a> </p>
    </footer>
  );
}

export default Footer;
