import Head from "next/head";
import Layout from "../components/Layout";

export default function servicios() {
  const servicios = [
    { icon: "tachometer-alt", title: "Kilometraje Libre", detail: "" },
    { icon: "piggy-bank", title: "El mejor Precio Asegurado", detail: "" },
    { icon: "headset", title: "Atencion Personalizada", detail: "" },
    { icon: "car", title: "Variedad y disponibilidad de unidades", detail: "" },
  ];
  const ademas = [
    { icon: "user-plus", title: "Autos con y sin chofer" },
    { icon: "users", title: "Traslados  de contingentes  " },
    {
      icon: "mountain",
      title: "Logística turística",
      detail: "Paseos de compra, circuitos turísticos, hoteles",
    },
    {
      icon: "briefcase",
      title: "Logística empresarial",
      detail: "Traslados VIP; recorridos de campo",
    },
    { icon: "building", title: "Logistica Urbana" },
    { icon: "space-shuttle", title: "Shuttle a hoteles y  aeropuertos" },
    {
      icon: "exchange-alt",
      title: "Dropoff (servicio One Way USH y RG)",
      detail: "Con equipamiento para temporada  invernal ",
    },
    { icon: "language", title: "Atencion trilingue español, francés, inglés." },
    {
      icon: "star",
      title: "Salon VIP en el aeropuerto de RGA y en el centro de la ciudad.",
    },
    { icon: "credit-card", title: "Pago con todo tipo de tarjetas" },
    {
      icon: "door-open",
      title: "Entrega y/o devolucion a domicilio – puerta a puerta -",
    },
    { icon: "smile", title: "Abierto todo el año, las  24hs " },
    {
      icon: "car-crash",
      title: "Seguro contra tercero full o CDW con franquicia",
      detail: "(Collision Damage Weaiver)",
    },
    {
      icon: "passport",
      title: "Con habilitacion de salida a Chile y venta del seguro Soapex",
    },
  ];
  return (
    <>
      <Head>
        <title>Servicios</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout>
        <div className="jumbotron">
          <h2>Somos su mejor opción</h2>
          <p> Nos destacamos de la competencia por esto: </p>
        </div>
        <ul>
          {servicios.map((servicio) => (
            <li key={servicio.title}>
              <i className="`fa fa-${servicio.icon} fa-5x`" />
              <h3>{servicio.title}</h3>
            </li>
          ))}
        </ul>
        <div className="card">
          <ul>
            {ademas.map((mas) => (
              <li>
                <i className="`fa fa-fw fa-2x fa-${mas.icon}`" />
                <div>
                  <h5>{mas.title} </h5>
                  <small>{mas.detail}</small>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </Layout>
    </>
  );
}
