import Head from 'next/head'
import Link from 'next/link'
import Layout from '../components/Layout'

export default function Home() {
  const  slides = [
    {title:'Alquiler de Vehículos' ,detail : 'Somos una empresa con trayectoria en Tierra del Fuego. Nuestra experiencia permirte brindarle un buen asesoramiento en temas de elección de caminos, partes meteorológicos , accesos, manejo y prevención.', link:'/nosotros'},
    {title:'Kilometraje Libre' ,detail : 'Senda Renting entiende las necesidades del empresario, turista o comerciante que requiere de este servicio y ofreciendo una atención personalizada.', link:'/servicios'},
    {title:'Los Mejores Precios' ,detail : 'La variedad en el parque automotor y fundamentalmente el valor agregado que supone la atención personalizada brindada a sus clientes, hacen de ésta una empresa reconocida en el área.', link:'/contacto'},
] 
  return (
    <>
      <Head>
        <title>Home</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout>
        {slides.map(slide =>
        <div>
            <h2>{slide.title}</h2>
            <p>{slide.detail}</p>
            <p><Link href={slide.link}><a>Ver más</a></Link></p>
        </div>)}
      </Layout>
    </>
  )
}
