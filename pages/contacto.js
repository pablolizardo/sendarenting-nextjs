import Head from 'next/head'
import Layout from '../components/Layout'

export default function contacto() {
  return (
    <>
      <Head>
        <title>Contacto</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout>
      <div className='jumbotron'>
      <h2>Contactese con nosotros</h2>
        <p>Estamos estratégicamente en el centro comercial de la ciudad de  Río Grande , permitiendo de esta manera tomar o devolver el vehículo sin que ello le genere pérdida de tiempo. Además, si gusta, puede recibir la unidad en Hoteles o Aeropuertos de la provincia, con recargo de drop off</p>
      </div>
      <div className='card'>
        <h3>Envíenos su consulta ó solicite su presupuesto</h3>
        <form>
          <label>Nombre <input placeholder='Nombre'/> </label>
          <label>Email <input type='email' placeholder='Email'/> </label>
          <label>Telefono <input type='tel' placeholder='Telefono'/> </label>
          <label>Desde <input type='date' placeholder='Desde'/> </label>
          <label>Hasta <input type='date' placeholder='Hasta'/> </label>
          <label>Tipo de Vehiculo 
            <select>
              <option>Vehiculo Chico A</option>
              <option>Vehiculo B</option>
              <option>Vehiculo C</option>
              <option>Vehiculo D</option>
            </select>
          </label>
          <button>Solicitar</button>
        </form>
      </div>
      
      </Layout>
    </>
  )
}
