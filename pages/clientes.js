import Head from "next/head";
import Jumbotron from "../components/Jumbotron";
import Layout from "../components/Layout";

export default function clientes() {
  const clientes = [
    "ypf.png",
    "conae.jpeg",
    "sherwin.jpg",
    "total.png",
    "southtrac.jpg",
    "osde.jpg",
    "supercemento.jpg",
    "gendarmeria.jpg",
    "securitas.png",
    "veng.jpeg",
    "nasa.jpeg",
    "schneider.jpeg",
    "roch.jpeg",
    "ultramar.png",
    "telespazio.jpg",
    "soki.png",
    "gie.jpeg",
    "netpetrol.jpeg",
    "kmg.jpeg",
    "gottert.jpeg",
  ];
  return (
    <>
      <Head>
        <title>Clientes</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout>
        <div className="jumbotron">
          <h2>Clientes</h2>
          <p>Éstos son sólo algunos de nuestros principales socios</p>
        </div>
        <div className='card'>
          <ul>
            {clientes.map((cliente, index) =>
            <li key={cliente}>
              <img
                src={`images/clients/${cliente}`}
                className="animated fadeIn"
                alt={cliente}
                style={{animationDelay : (index*0.15)+.5 + 's' }}
              />
            </li>
            )}
          </ul>
        </div>
      </Layout>
    </>
  );
}
