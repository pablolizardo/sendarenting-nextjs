import Head from "next/head";
import Layout from "../components/Layout";

export default function nosotros() {
  return (
    <>
      <Head>
        <title>Nosotros</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout>
        <img src="images/equipo/senda.jpeg" />
        <div className="jumbotron">
          <h2>Nosotros</h2>
          <p>
            Sendas Renting es una empresa, con una trayectoria de más de doce
            años en el rubro, sus agradables instalaciones tanto en el local de
            AA2000 , como en la oficina del centro, te permitirán acceder de
            manera cómoda y rápida a tomar tu unidad y disfrutar de la Tierra
            del Fuego. Te ofrecemos una atención personalizada, para que te
            sientas cómodo y confiado.
          </p>
        </div>
        <img src="images/eco_fren.png" />
        <div className="card">
          <img src="images/equipo/equipo.jpeg" />
        </div>
      </Layout>
    </>
  );
}
